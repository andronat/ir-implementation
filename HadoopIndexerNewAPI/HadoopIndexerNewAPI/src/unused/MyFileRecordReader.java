/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package unused;

import java.io.IOException;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.CombineFileSplit;

/**
 *
 * @author Anastasis Andronidis <anastasis90@yahoo.gr>
 */
public class MyFileRecordReader extends RecordReader<Text, Text> {

    private long fileLength;
    private FileSystem fs;
    private Path path;
    private Text key = null;
    private Text value = null;
    private boolean processed = false;

    public MyFileRecordReader(CombineFileSplit split, TaskAttemptContext context, Integer index) throws IOException {
        this.fs = FileSystem.get(context.getConfiguration());
        this.path = split.getPath(index);
        this.fileLength = split.getLength(index);
    }

    @Override
    public void initialize(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
    }

    @Override
    public void close() throws IOException {
    }

    @Override
    public float getProgress() throws IOException {
        return processed ? 1.0f : 0.0f;
    }

    @Override
    public boolean nextKeyValue() throws IOException {

        if (!processed) {
            byte[] contents = new byte[(int) this.fileLength];
            FSDataInputStream in = null;
            try {

                this.key = new Text();
                this.key.set(path.getName());

                in = this.fs.open(path);
                IOUtils.readFully(in, contents, 0, contents.length);
                this.value = new Text();
                this.value.set(contents, 0, contents.length);

            } finally {
                IOUtils.closeStream(in);
            }
            processed = true;
            return true;
        }
        return false;
    }

    @Override
    public Text getCurrentKey() throws IOException, InterruptedException {
        return key;
    }

    @Override
    public Text getCurrentValue() throws IOException, InterruptedException {
        return value;
    }
}
