/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hadoopindexer;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author Anastasis Andronidis <anastasis90@yahoo.gr>
 */
public class WordCombiner extends Reducer<Text, Text, Text, Text> {
    
    Text value = new Text();
    
    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

        StringBuilder sb = new StringBuilder(1000);
        Set<String> docSet = new HashSet<String>();
        
        for(Text t : values) {
            docSet.add(t.toString());
        }
        
        for(String doc: docSet) {
            sb.append(" ").append(doc);
        }
        
        value.set(sb.toString());
        context.write(key, value);
    }
}
