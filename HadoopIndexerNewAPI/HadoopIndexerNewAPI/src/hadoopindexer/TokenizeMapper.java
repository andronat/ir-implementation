/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hadoopindexer;

import java.io.IOException;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import utils.StopWords;

/**
 *
 * @author Anastasis Andronidis <anastasis90@yahoo.gr>
 */
public class TokenizeMapper extends Mapper<Text, Text, Text, Text> {

    private Text word = new Text();

//    private String punctuationClean(String st) {
//        return st.replaceAll("\\+|\\<|\\=|\\>|\\`|\\||\\~|\\^|\\*|\\]|\\[|\\%|\\@|\\#|\\$|\\.|\\,|\\:|\\;|\\\"|\\!|\\?|\\_|\\(|\\)|\\&|\\{|\\}|\\/|\\-|\\'", "");//.replaceAll("(\'|\\-)([a-zA-Z0-9\\-\\']+)(\'|\\-)", "$2").replaceAll("(^| )(\'|\\-)([a-zA-Z0-9\\-\\']+ )", "$3").replaceAll("( [a-zA-Z0-9\\-\\']+)(\'|\\-)( )", "$1$3");
//    }

    @Override
    public void map(Text key, Text value, Context context) throws IOException, InterruptedException {

        // Removing puctuation
        //String bigString = punctuationClean(new String(value.getBytes()));

        //StringTokenizer itr = new StringTokenizer(bigString);

        //HashSet<String> wordSet = new HashSet<String>();

        HashSet<String> stopWords = StopWords.GetStopWordsSet();

        Pattern p = Pattern.compile("\\w+");
        Matcher m = p.matcher(value.toString());

        String tmp_word;
        while (m.find()) {
            tmp_word = m.group().toLowerCase();

            if (!Character.isLetter(tmp_word.charAt(0)) || Character.isDigit(tmp_word.charAt(0)) || tmp_word.contains("_") || stopWords.contains(tmp_word) || tmp_word.matches(".*[0-9].*")){
                continue;
            } 

            word.set(tmp_word);
            context.write(word, key);
//            wordSet.add(tmp_word);
        }

//        for (String wd : wordSet) {
//            word.set(wd);
//            context.write(word, key);
//        }
    }
}
