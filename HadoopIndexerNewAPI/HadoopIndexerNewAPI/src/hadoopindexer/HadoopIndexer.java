/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hadoopindexer;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import utils.JobBuilder;

/**
 *
 * @author Anastasis Andronidis <anastasis90@yahoo.gr>
 */
public class HadoopIndexer extends Configured implements Tool {

    @Override
    public int run(String[] args) throws Exception {

        Job job = JobBuilder.parseInputAndOutput(this, getConf(), args);
        if (job == null) {
            return -1;
        }

//        job.setInputFormatClass(KeyValueTextInputFormat.class);
        job.setInputFormatClass(SequenceFileInputFormat.class);
//        job.setOutputFormatClass(SequenceFileOutputFormat.class);
        
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        job.setMapperClass(TokenizeMapper.class);
        job.setCombinerClass(WordCombiner.class);
        job.setReducerClass(MakeInvertedIndexReducer.class);
        
        //job.getConfiguration().setLong("mapred.max.split.size", (long)532000);
        //job.getConfiguration().setLong("key.value.separator.in.input.line", '\t');
//        KeyValueTextInputFormat.setMaxInputSplitSize(job, (long)1932000);
        job.setNumReduceTasks(1);
       
        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] args) throws Exception {
        int ret = ToolRunner.run(new HadoopIndexer(), args);
        System.exit(ret);
    }
}