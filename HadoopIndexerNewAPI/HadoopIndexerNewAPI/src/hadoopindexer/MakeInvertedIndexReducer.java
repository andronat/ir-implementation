/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hadoopindexer;

import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author Anastasis Andronidis <anastasis90@yahoo.gr>
 */
public class MakeInvertedIndexReducer extends Reducer<Text, Text, Text, Text> {

    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

        StringBuilder sb = new StringBuilder(1000);
        
        for (Text val : values) {
            sb.append(" ").append(val.toString());
        }
        
        context.write(key, new Text(sb.toString()));
    }
}