// $ANTLR 3.4 /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g 2012-06-22 00:44:41

package search_modules.boolean_model;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class LogicLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__6=6;
    public static final int T__7=7;
    public static final int T__8=8;
    public static final int T__9=9;
    public static final int T__10=10;
    public static final int T__11=11;
    public static final int ID=4;
    public static final int Space=5;

    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public LogicLexer() {} 
    public LogicLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public LogicLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "/Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g"; }

    // $ANTLR start "T__6"
    public final void mT__6() throws RecognitionException {
        try {
            int _type = T__6;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:7:6: ( '(' )
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:7:8: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__6"

    // $ANTLR start "T__7"
    public final void mT__7() throws RecognitionException {
        try {
            int _type = T__7;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:8:6: ( ')' )
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:8:8: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__7"

    // $ANTLR start "T__8"
    public final void mT__8() throws RecognitionException {
        try {
            int _type = T__8;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:9:6: ( '->' )
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:9:8: '->'
            {
            match("->"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__8"

    // $ANTLR start "T__9"
    public final void mT__9() throws RecognitionException {
        try {
            int _type = T__9;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:10:6: ( 'AND' )
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:10:8: 'AND'
            {
            match("AND"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__9"

    // $ANTLR start "T__10"
    public final void mT__10() throws RecognitionException {
        try {
            int _type = T__10;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:11:7: ( 'NOT' )
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:11:9: 'NOT'
            {
            match("NOT"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__10"

    // $ANTLR start "T__11"
    public final void mT__11() throws RecognitionException {
        try {
            int _type = T__11;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:12:7: ( 'OR' )
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:12:9: 'OR'
            {
            match("OR"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__11"

    // $ANTLR start "ID"
    public final void mID() throws RecognitionException {
        try {
            int _type = ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:43:7: ( ( 'a' .. 'z' | 'A' .. 'Z' )+ )
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:43:9: ( 'a' .. 'z' | 'A' .. 'Z' )+
            {
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:43:9: ( 'a' .. 'z' | 'A' .. 'Z' )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0 >= 'A' && LA1_0 <= 'Z')||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:
            	    {
            	    if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ID"

    // $ANTLR start "Space"
    public final void mSpace() throws RecognitionException {
        try {
            int _type = Space;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:44:7: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:44:9: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:44:9: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0 >= '\t' && LA2_0 <= '\n')||LA2_0=='\r'||LA2_0==' ') ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:
            	    {
            	    if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);


            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Space"

    public void mTokens() throws RecognitionException {
        // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:1:8: ( T__6 | T__7 | T__8 | T__9 | T__10 | T__11 | ID | Space )
        int alt3=8;
        switch ( input.LA(1) ) {
        case '(':
            {
            alt3=1;
            }
            break;
        case ')':
            {
            alt3=2;
            }
            break;
        case '-':
            {
            alt3=3;
            }
            break;
        case 'A':
            {
            int LA3_4 = input.LA(2);

            if ( (LA3_4=='N') ) {
                int LA3_9 = input.LA(3);

                if ( (LA3_9=='D') ) {
                    int LA3_12 = input.LA(4);

                    if ( ((LA3_12 >= 'A' && LA3_12 <= 'Z')||(LA3_12 >= 'a' && LA3_12 <= 'z')) ) {
                        alt3=7;
                    }
                    else {
                        alt3=4;
                    }
                }
                else {
                    alt3=7;
                }
            }
            else {
                alt3=7;
            }
            }
            break;
        case 'N':
            {
            int LA3_5 = input.LA(2);

            if ( (LA3_5=='O') ) {
                int LA3_10 = input.LA(3);

                if ( (LA3_10=='T') ) {
                    int LA3_13 = input.LA(4);

                    if ( ((LA3_13 >= 'A' && LA3_13 <= 'Z')||(LA3_13 >= 'a' && LA3_13 <= 'z')) ) {
                        alt3=7;
                    }
                    else {
                        alt3=5;
                    }
                }
                else {
                    alt3=7;
                }
            }
            else {
                alt3=7;
            }
            }
            break;
        case 'O':
            {
            int LA3_6 = input.LA(2);

            if ( (LA3_6=='R') ) {
                int LA3_11 = input.LA(3);

                if ( ((LA3_11 >= 'A' && LA3_11 <= 'Z')||(LA3_11 >= 'a' && LA3_11 <= 'z')) ) {
                    alt3=7;
                }
                else {
                    alt3=6;
                }
            }
            else {
                alt3=7;
            }
            }
            break;
        case 'B':
        case 'C':
        case 'D':
        case 'E':
        case 'F':
        case 'G':
        case 'H':
        case 'I':
        case 'J':
        case 'K':
        case 'L':
        case 'M':
        case 'P':
        case 'Q':
        case 'R':
        case 'S':
        case 'T':
        case 'U':
        case 'V':
        case 'W':
        case 'X':
        case 'Y':
        case 'Z':
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
        case 'g':
        case 'h':
        case 'i':
        case 'j':
        case 'k':
        case 'l':
        case 'm':
        case 'n':
        case 'o':
        case 'p':
        case 'q':
        case 'r':
        case 's':
        case 't':
        case 'u':
        case 'v':
        case 'w':
        case 'x':
        case 'y':
        case 'z':
            {
            alt3=7;
            }
            break;
        case '\t':
        case '\n':
        case '\r':
        case ' ':
            {
            alt3=8;
            }
            break;
        default:
            NoViableAltException nvae =
                new NoViableAltException("", 3, 0, input);

            throw nvae;

        }

        switch (alt3) {
            case 1 :
                // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:1:10: T__6
                {
                mT__6(); 


                }
                break;
            case 2 :
                // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:1:15: T__7
                {
                mT__7(); 


                }
                break;
            case 3 :
                // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:1:20: T__8
                {
                mT__8(); 


                }
                break;
            case 4 :
                // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:1:25: T__9
                {
                mT__9(); 


                }
                break;
            case 5 :
                // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:1:30: T__10
                {
                mT__10(); 


                }
                break;
            case 6 :
                // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:1:36: T__11
                {
                mT__11(); 


                }
                break;
            case 7 :
                // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:1:42: ID
                {
                mID(); 


                }
                break;
            case 8 :
                // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:1:45: Space
                {
                mSpace(); 


                }
                break;

        }

    }


 

}