// $ANTLR 3.4 /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g 2012-06-22 00:44:40

package search_modules.boolean_model;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class LogicParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ID", "Space", "'('", "')'", "'->'", "'AND'", "'NOT'", "'OR'"
    };

    public static final int EOF=-1;
    public static final int T__6=6;
    public static final int T__7=7;
    public static final int T__8=8;
    public static final int T__9=9;
    public static final int T__10=10;
    public static final int T__11=11;
    public static final int ID=4;
    public static final int Space=5;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public LogicParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public LogicParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected TreeAdaptor adaptor = new CommonTreeAdaptor();

public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = adaptor;
}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}
    public String[] getTokenNames() { return LogicParser.tokenNames; }
    public String getGrammarFileName() { return "/Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g"; }


    public static class parse_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "parse"
    // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:12:1: parse : expression EOF !;
    public final LogicParser.parse_return parse() throws RecognitionException {
        LogicParser.parse_return retval = new LogicParser.parse_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token EOF2=null;
        LogicParser.expression_return expression1 =null;


        CommonTree EOF2_tree=null;

        try {
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:13:3: ( expression EOF !)
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:13:6: expression EOF !
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_expression_in_parse43);
            expression1=expression();

            state._fsp--;

            adaptor.addChild(root_0, expression1.getTree());

            EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_parse45); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "parse"


    public static class expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "expression"
    // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:16:1: expression : implication ;
    public final LogicParser.expression_return expression() throws RecognitionException {
        LogicParser.expression_return retval = new LogicParser.expression_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        LogicParser.implication_return implication3 =null;



        try {
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:17:3: ( implication )
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:17:6: implication
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_implication_in_expression64);
            implication3=implication();

            state._fsp--;

            adaptor.addChild(root_0, implication3.getTree());

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "expression"


    public static class implication_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "implication"
    // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:20:1: implication : or ( '->' ^ or )* ;
    public final LogicParser.implication_return implication() throws RecognitionException {
        LogicParser.implication_return retval = new LogicParser.implication_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal5=null;
        LogicParser.or_return or4 =null;

        LogicParser.or_return or6 =null;


        CommonTree string_literal5_tree=null;

        try {
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:21:3: ( or ( '->' ^ or )* )
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:21:6: or ( '->' ^ or )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_or_in_implication78);
            or4=or();

            state._fsp--;

            adaptor.addChild(root_0, or4.getTree());

            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:21:9: ( '->' ^ or )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==8) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:21:10: '->' ^ or
            	    {
            	    string_literal5=(Token)match(input,8,FOLLOW_8_in_implication81); 
            	    string_literal5_tree = 
            	    (CommonTree)adaptor.create(string_literal5)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(string_literal5_tree, root_0);


            	    pushFollow(FOLLOW_or_in_implication84);
            	    or6=or();

            	    state._fsp--;

            	    adaptor.addChild(root_0, or6.getTree());

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "implication"


    public static class or_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "or"
    // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:24:1: or : and ( 'OR' ^ and )* ;
    public final LogicParser.or_return or() throws RecognitionException {
        LogicParser.or_return retval = new LogicParser.or_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal8=null;
        LogicParser.and_return and7 =null;

        LogicParser.and_return and9 =null;


        CommonTree string_literal8_tree=null;

        try {
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:25:3: ( and ( 'OR' ^ and )* )
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:25:6: and ( 'OR' ^ and )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_and_in_or104);
            and7=and();

            state._fsp--;

            adaptor.addChild(root_0, and7.getTree());

            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:25:10: ( 'OR' ^ and )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==11) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:25:11: 'OR' ^ and
            	    {
            	    string_literal8=(Token)match(input,11,FOLLOW_11_in_or107); 
            	    string_literal8_tree = 
            	    (CommonTree)adaptor.create(string_literal8)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(string_literal8_tree, root_0);


            	    pushFollow(FOLLOW_and_in_or110);
            	    and9=and();

            	    state._fsp--;

            	    adaptor.addChild(root_0, and9.getTree());

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "or"


    public static class and_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "and"
    // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:28:1: and : not ( 'AND' ^ not )* ;
    public final LogicParser.and_return and() throws RecognitionException {
        LogicParser.and_return retval = new LogicParser.and_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal11=null;
        LogicParser.not_return not10 =null;

        LogicParser.not_return not12 =null;


        CommonTree string_literal11_tree=null;

        try {
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:29:3: ( not ( 'AND' ^ not )* )
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:29:6: not ( 'AND' ^ not )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_not_in_and130);
            not10=not();

            state._fsp--;

            adaptor.addChild(root_0, not10.getTree());

            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:29:10: ( 'AND' ^ not )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==9) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:29:11: 'AND' ^ not
            	    {
            	    string_literal11=(Token)match(input,9,FOLLOW_9_in_and133); 
            	    string_literal11_tree = 
            	    (CommonTree)adaptor.create(string_literal11)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(string_literal11_tree, root_0);


            	    pushFollow(FOLLOW_not_in_and136);
            	    not12=not();

            	    state._fsp--;

            	    adaptor.addChild(root_0, not12.getTree());

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "and"


    public static class not_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "not"
    // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:32:1: not : ( 'NOT' ^ atom | atom );
    public final LogicParser.not_return not() throws RecognitionException {
        LogicParser.not_return retval = new LogicParser.not_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal13=null;
        LogicParser.atom_return atom14 =null;

        LogicParser.atom_return atom15 =null;


        CommonTree string_literal13_tree=null;

        try {
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:33:3: ( 'NOT' ^ atom | atom )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==10) ) {
                alt4=1;
            }
            else if ( (LA4_0==ID||LA4_0==6) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;

            }
            switch (alt4) {
                case 1 :
                    // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:33:6: 'NOT' ^ atom
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal13=(Token)match(input,10,FOLLOW_10_in_not158); 
                    string_literal13_tree = 
                    (CommonTree)adaptor.create(string_literal13)
                    ;
                    root_0 = (CommonTree)adaptor.becomeRoot(string_literal13_tree, root_0);


                    pushFollow(FOLLOW_atom_in_not161);
                    atom14=atom();

                    state._fsp--;

                    adaptor.addChild(root_0, atom14.getTree());

                    }
                    break;
                case 2 :
                    // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:34:6: atom
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_atom_in_not172);
                    atom15=atom();

                    state._fsp--;

                    adaptor.addChild(root_0, atom15.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "not"


    public static class atom_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "atom"
    // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:37:1: atom : ( ID | '(' ! expression ')' !);
    public final LogicParser.atom_return atom() throws RecognitionException {
        LogicParser.atom_return retval = new LogicParser.atom_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token ID16=null;
        Token char_literal17=null;
        Token char_literal19=null;
        LogicParser.expression_return expression18 =null;


        CommonTree ID16_tree=null;
        CommonTree char_literal17_tree=null;
        CommonTree char_literal19_tree=null;

        try {
            // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:38:3: ( ID | '(' ! expression ')' !)
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==ID) ) {
                alt5=1;
            }
            else if ( (LA5_0==6) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;

            }
            switch (alt5) {
                case 1 :
                    // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:38:6: ID
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    ID16=(Token)match(input,ID,FOLLOW_ID_in_atom186); 
                    ID16_tree = 
                    (CommonTree)adaptor.create(ID16)
                    ;
                    adaptor.addChild(root_0, ID16_tree);


                    }
                    break;
                case 2 :
                    // /Users/mk/Desktop/?????/8o ???????/???????? ???????????/ir-implementation/ARES_Project/src/boolean_model/Logic.g:39:6: '(' ! expression ')' !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal17=(Token)match(input,6,FOLLOW_6_in_atom193); 

                    pushFollow(FOLLOW_expression_in_atom196);
                    expression18=expression();

                    state._fsp--;

                    adaptor.addChild(root_0, expression18.getTree());

                    char_literal19=(Token)match(input,7,FOLLOW_7_in_atom198); 

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "atom"

    // Delegated rules


 

    public static final BitSet FOLLOW_expression_in_parse43 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_parse45 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_implication_in_expression64 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_or_in_implication78 = new BitSet(new long[]{0x0000000000000102L});
    public static final BitSet FOLLOW_8_in_implication81 = new BitSet(new long[]{0x0000000000000450L});
    public static final BitSet FOLLOW_or_in_implication84 = new BitSet(new long[]{0x0000000000000102L});
    public static final BitSet FOLLOW_and_in_or104 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_11_in_or107 = new BitSet(new long[]{0x0000000000000450L});
    public static final BitSet FOLLOW_and_in_or110 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_not_in_and130 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_9_in_and133 = new BitSet(new long[]{0x0000000000000450L});
    public static final BitSet FOLLOW_not_in_and136 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_10_in_not158 = new BitSet(new long[]{0x0000000000000050L});
    public static final BitSet FOLLOW_atom_in_not161 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_atom_in_not172 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_atom186 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_6_in_atom193 = new BitSet(new long[]{0x0000000000000450L});
    public static final BitSet FOLLOW_expression_in_atom196 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_7_in_atom198 = new BitSet(new long[]{0x0000000000000002L});

}