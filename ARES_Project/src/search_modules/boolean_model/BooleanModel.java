/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package search_modules.boolean_model;

import ares_project.InvertedIndex;
import ares_project.utilities.F2S;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import search_modules.SearchModel;

/**
 *
 * @author mk
 */
public class BooleanModel extends SearchModel {

    /**
     *
     * @param ii
     */
    public BooleanModel(InvertedIndex ii) {
        super(ii);
    }

    /**
     * Όπως λέει και το όνομα της εδώ γίνεται όλη η σκληρή δουλειά. Παίρνουμε
     * την έκφραση του δέντρου που δημιουργήσαμε νωρίτερα και χωρίζουμε τις
     * μεταβλητές. Παίρνουμε την κάθε παρένθεση και ανάλογα με με το πόσες
     * έχουμε πράτουμε ανάλογα. Για παράδειγμα η δυνατές επιλογές που μπορούμε
     * να έχουμε είναι 1,2 η καμιά παρένθεση. Ανάλογα με την περίπτωση καλούμε
     * και την συνάρτηση που αντιμετωπίζει την περίπτωση.
     */
    private List<Integer> HardWork(String tempo) {

        List<Integer> resultSet = new ArrayList<Integer>();
        String asda = tempo;
        int lpar = 0;
        int p1 = 0, p2 = 0, p3 = 0, p4 = 0;
        boolean einai = false;
        boolean einai2 = false;
        boolean einai3 = false;
        boolean einai4 = false;
        boolean einai5 = false;

        //## Εδώ έχουμε τον καθορισμό των παρενθέσεων

        for (int i = 1; i < tempo.length(); i++) {
            if (tempo.charAt(i) == '(' && !einai2) {
                lpar++;
                if (!einai4) {                    //NEW CODE FOR p1
                    p1 = i;
                    einai4 = true;
                }
                einai = true;
            }
            if (tempo.charAt(i) == ')') {
                lpar--;
            }
            if (lpar == 0 && einai) {
                p2 = i + 1;
                einai = false;
                einai2 = true;
            }
            if (tempo.charAt(i) == '(' && einai2) {
                lpar++;
                if (!einai5) {                    //NEW CODE FOR p3
                    p3 = i;
                    einai5 = true;
                }
                einai3 = true;
            }
            if (lpar == 0 && einai3) {
                p4 = i + 1;
                break;
            }
        }

        String ss = tempo.substring(p1, p2);
        String ss2 = tempo.substring(p3, p4);

        //## Ανάλογα με τις παρενθέσεις εκτελούμε και άλλη μέθοδο. Σε περίπτωση που δεν 
        //## έχουμε παρενθέσεις και έχουμε 2 όρους αποκλειστικά τότε εκτελούμε την 
        //## ProcessBooleanOperator.

        if (ss.length() == 0 && ss2.length() == 0) {

            resultSet = ProcessBooleanOperator(tempo);                          //DONE DOULEUEI
            System.out.println();
        } //## Στην περίπτωση που έχουμε 1 παρένθεση τότε προσδιορίουμε που βρίσκεται ο 2ος
        //## όρος εκτός της παρένθεσης. Και ανάλογα παίρνουμε τα στοιχεία καλώντας ξανά την
        //## HardWork και στην συνέχεια την ProcessBooleanOperator2 όπου κάνει τον 
        //## έλεγχο μεταξύ μιας λίστας και ενώς String.
        else if (ss.length() > 0 && ss2.length() == 0) {                      //DONE GAMWTO NAI!

            int er = p2;

            int er2 = 0;
            int temposiz = tempo.length();

            for (int rer = er - 1; rer < temposiz; rer++) {
                if (tempo.charAt(rer) == ')') {
                    er2++;
                }
            }

            if (er2 == tempo.length() - p2 + 1) {
                List<Integer> t1 = new ArrayList<Integer>();
                t1 = HardWork(ss);

                String opi = tempo.substring(1, 4);                 //p1-1 eixa edw
                String opt2 = tempo.substring(4, p1 - 1);
                resultSet = ProcessBooleanOperator2(opi, t1, opt2);
            } else {
                List<Integer> t1 = new ArrayList<Integer>();
                t1 = HardWork(ss);
                String opi = tempo.substring(1, p1 - 1);                 //p1-1 eixa edw
                String opt2 = tempo.substring(p2, tempo.length() - 1);
                resultSet = ProcessBooleanOperator2(opi, t1, opt2);
            }

            System.out.println();
        } else if (ss.length() > 0 && ss2.length() > 0) {
            
            List<Integer> t1 = new ArrayList<Integer>();
            List<Integer> t2 = new ArrayList<Integer>();

            t1 = HardWork(ss);
            t2 = HardWork(ss2);

            String opi = tempo.substring(1, p1 - 1);
            resultSet = ProcessBooleanOperator3(opi, t1, t2);
            
        }

        return resultSet;
    }

    /**
     * Βασική μέθοδος αναζήτης. Εδώ έρχεται το ερώτημα του χρήστη όπου και
     * μετατρέπεται σε δέντρο. Στην συνέχεια εκτελείται η μέθοδος HardWork.
     */
    private List<Integer> Searchor(String list) {
        
        List<Integer> fin2;
        LogicLexer lexer = new LogicLexer(new ANTLRStringStream(list));
        LogicParser parser = new LogicParser(new CommonTokenStream(lexer));
        CommonTree tree = null;

        try {
            tree = (CommonTree) parser.parse().getTree();
        } catch (RecognitionException ex) {
            Logger.getLogger(BooleanModel.class.getName()).log(Level.SEVERE, null, ex);
        }

        String tempo = tree.toStringTree();

        fin2 = HardWork(tempo);
        return fin2;
    }

    //returns term incidence vector
    /**
     *
     * @param term = String της λέξης που αναζητούμε.
     * @return Επιστρέφει μια λίστα, όπου η λίστα αυτή δείχνει σε ποιά έγγραφα
     * περιέχεται η λέξη.
     */
    public List<Integer> GetTermIncidenceVector(String term) {
        
        HashMap<Integer, Integer> hm = ii.searchTerm(term);

        //System.out.println(hm);

        List<Integer> asd = new ArrayList<Integer>();

        try {

            for (Map.Entry<Integer, Integer> entry : hm.entrySet()) {
                asd.add(entry.getKey());
            }

        } catch (NullPointerException e) {
        }
        return asd;
    }

    /**
     * Εδώ είναι η πρώτη μέθοδος για να βρούμε το αποτέλεσμα μεταξύ 2 λέξεων.
     * Χωρίζουμε το String παίρνωντας τους 2 όρους. Και στην συνέχεια με τον
     * τελεστή που έδωσε ο χρήστης γίνεται και η ανάλογη ενέργεια. ( AND OR )
     *
     * @param op
     * @return
     */
    public List<Integer> ProcessBooleanOperator(String op) {

        String list;

        if (op.charAt(0) == '(') {
            list = op.substring(1, op.length() - 1);
        } else {
            list = op;
        }

        String[] chaos = list.split(" ");
        int intr = chaos.length;

        List<Integer> term1 = new ArrayList<Integer>();
        List<Integer> term2 = new ArrayList<Integer>();
        List<Integer> temp = new ArrayList<Integer>();

        if (intr > 2) {

            term1 = GetTermIncidenceVector(chaos[1]);
            term2 = GetTermIncidenceVector(chaos[2]);


            if (chaos[0].equalsIgnoreCase("AND")) {

                term1.retainAll(term2);

            }
            if (chaos[0].equalsIgnoreCase("OR")) {
                for (Integer num : term2) {
                    if (term1.contains(num)) {
                    } else {
                        term1.add(num);
                    }
                }
            }
            temp = term1;
        } else if (intr == 2) {
            if (chaos[0].equalsIgnoreCase("NOT")) {
                Set nod = ii.getAllDocs();

                for (Object sd : nod) {
                    Integer x = (Integer) sd;
                    term1.add(x);
                }

                term2 = GetTermIncidenceVector(chaos[1]);
                term1.removeAll(term2);
                temp = term1;
            }
        } else if (intr < 2) {
            temp = GetTermIncidenceVector(chaos[0]);
        }

        return temp;
    }

    /**
     * Εδώ είναι η τρίτη μέθοδος για να βρούμε το αποτέλεσμα μεταξύ 2 λίστων.
     * Σύμφωνα με τον τελεστή που έδωσε ο χρήστης γίνεται και η ανάλογη
     * ενέργεια. ( AND OR )
     *
     * @param op
     * @param l1
     * @param l2
     * @return
     */
    public List<Integer> ProcessBooleanOperator3(String op, List l1, List l2) {

        List<Integer> term2 = new ArrayList<Integer>();
        List<Integer> temp = new ArrayList<Integer>();
        temp = l1;
        term2 = l2;

        if (op.equalsIgnoreCase("AND")) {
            temp.retainAll(term2);
        }
        if (op.equalsIgnoreCase("OR")) {
            for (Integer num : term2) {
                if (temp.contains(num)) {
                } else {
                    temp.add(num);
                }
            }
        }
        return temp;
    }

    /**
     *
     * Εδώ είναι η δεύτερη μέθοδος για να βρούμε το αποτέλεσμα μεταξύ 1 λέξης
     * και 1 λίστας. σύμφωνα με τον τελεστή που έδωσε ο χρήστης γίνεται και η
     * ανάλογη ενέργεια. ( AND OR )
     *
     * @param op
     * @param l1
     * @param s
     * @return
     */
    public List<Integer> ProcessBooleanOperator2(String op, List l1, String s) {

        List resultSet = new ArrayList<Integer>();
        String list;

        if (op.charAt(0) == '(') {
            list = op.substring(1, op.length() - 1);
        } else {
            list = op;
        }

        String opi = op.trim();
        String ser = s;

        List<Integer> term1 = new ArrayList<Integer>();
        List<Integer> term2 = new ArrayList<Integer>();
        List<Integer> temp = new ArrayList<Integer>();

        term1 = GetTermIncidenceVector(ser.trim());
        term2 = l1;

        if (opi.equalsIgnoreCase("AND")) {
            term1.retainAll(term2);
        }
        if (opi.equals("OR")) {
            for (Integer num : term2) {
                if (term1.contains(num)) {
                } else {
                    term1.add(num);
                }
            }
        }
        return term1;
    }

    static String askForUserQuery() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nGive query:\n");
        return scanner.nextLine();
    }

    /**
     * Λειτουργία εισαγωγής ερωτήματος από τον χρήστη.
     */
    @Override
    public void userQuery() {
        List<Integer> results = Searchor(askForUserQuery());
        System.out.println("List of documents : " + results);
    }

    /**
     * Λειτουργία για εισαγωγή ερωτήματος από αρχείο.
     */
    @Override
    public void fileQuery() {
        
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nGive the path of the input file:\n");
        String path = scanner.nextLine();
        BufferedReader br;
        String strLine;


        try {
            FileInputStream fstream = new FileInputStream(path);
            DataInputStream in = new DataInputStream(fstream);
            br = new BufferedReader(new InputStreamReader(in));
            int i = 1;

                    while ((strLine = br.readLine()) != null) {

                        List<Integer> results = Searchor(strLine);
                        System.out.println("List of documents for query:"+i+" :"+results);
                        i++;
                    }
                    
                    
            in.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }
}
