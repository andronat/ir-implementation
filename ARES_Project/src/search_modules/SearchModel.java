/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package search_modules;

import ares_project.InvertedIndex;

/**
 *
 * @author Anastasis Andronidis <anastasis90@yahoo.gr>
 */
public abstract class SearchModel {
    
    protected InvertedIndex ii;
    
    public SearchModel(InvertedIndex ii) {
        this.ii = ii;
    }
    
    public abstract void userQuery();
    
    public abstract void fileQuery();
}
