/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package search_modules.vector_space_model;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Ilias Trichopoulos <hliastrich@yahoo.gr>
 */
 class Statistics {
     
    private HashMap<Integer, List<Integer>> relevant;   //ie: (query)1 => (documents)[13,14,15,17,20]
    
    
    public Statistics(String library){
        
        relevant = new HashMap<Integer, List<Integer>>();
        
        // parse from the file and initialize the HashMap
        try {
            FileInputStream fstream = new FileInputStream(library);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String nextLine;
            
            while((nextLine = br.readLine()) != null){
                String [] tokens = nextLine.split(" ");
                int queryID = Integer.parseInt(tokens[0]);
                int documentID = Integer.parseInt(tokens[1]);
                
                if(relevant.containsKey(queryID)){
                    relevant.get(queryID).add(documentID);
                }
                else{
                    List<Integer> relevantList = new ArrayList<Integer>();
                    relevantList.add(documentID);
                    relevant.put(queryID, relevantList);
                }
            }
            
            in.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }
    
    public float getPrecision(int queryID, List<Integer> returned){
        
        int relevantReturned = calculateReleventReturned(queryID,returned);
        float precision = (float)relevantReturned/returned.size();
        
        return precision;
    }
    
    public float getRecall(int queryID, List<Integer> returned){
        
        int relevantReturned = calculateReleventReturned(queryID,returned);
        float recall = (float)relevantReturned/relevant.get(queryID).size();
        
        return recall;
    }
    
    private int calculateReleventReturned(int queryID, List<Integer> returned){
        
        int relevantReturned = 0;
        for (int i = 0; i < returned.size(); i++) {
            if (relevant.get(queryID).contains(returned.get(i))) {
                relevantReturned++;
            }
        }
        return relevantReturned;
    }
}
