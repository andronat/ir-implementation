/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package search_modules.vector_space_model;

import ares_project.InvertedIndex;
import ares_project.utilities.F2S;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.Map.Entry;
import search_modules.SearchModel;

/**
 *
 * @author ilias
 */
public class VectorSpaceModel extends SearchModel {

    private int k = 0;
    
    private Statistics statistics;

    public VectorSpaceModel(InvertedIndex ii) {
        super(ii);
    }

    private ArrayList<Integer> calculationMethod1(List<String> list) { // the list contains all the terms of the query

        HashMap<Integer, Double> similarities = new HashMap<Integer, Double>();        //docNum,w
//        HashMap<Integer, Double> L = new HashMap<Integer, Double>();

        // gia ka8e oro
        for (int i = 0; i < list.size(); i++) {
            String term = list.get(i);

            HashMap<Integer, Integer> hm = ii.searchTerm(term);
            if (hm != null) {

                
                if (!hm.isEmpty()) {    // if the term exists in the inderted index

                    // calculate the IDF
                    int nt = hm.size();
                    int N = ii.getNumberOfDocuments();
                    double IDF = Math.log(N / nt);      // method 1: 1st logarithmic formation

                    // calculate the TFs

                    // Find max freq.
                    Integer max = new Integer(Integer.MIN_VALUE);
                    for (Entry<Integer, Integer> entry : hm.entrySet()) {
                        if (entry.getValue() > max) {
                            max = entry.getValue();
                        }
                    }
                    for (Entry<Integer, Integer> entry : hm.entrySet()) {
                        double tmp = (double) entry.getValue() / max;   // method 1: normalized formation
                        double w = tmp * IDF;
                        if (similarities.containsKey(entry.getKey())) {
                            Double freq = similarities.remove(entry.getKey());
                            freq += w;
                            similarities.put(entry.getKey(), freq);
//                            Double Lsum = L.remove(entry.getKey());
//                            Lsum += Math.pow(w, 2);
//                            L.put(entry.getKey(), Lsum);
                        } else {
                            similarities.put(entry.getKey(), w);
//                            L.put(entry.getKey(), Math.pow(w, 2));
                        }
                    }
                }
            }
        }

        HashSet<Double> similarityScoresUnique = new HashSet<Double>();

        for (Entry<Integer, Double> entry : similarities.entrySet()) {
            double tmp2 = ii.getUniqTermsForDoc(entry.getKey());
            double docWeight = (double) entry.getValue() / tmp2; //ii.getLengthForDoc(entry.getKey());
//            double docWeight = (double) entry.getValue() / Math.sqrt(L.get(entry.getKey()));
            if(docWeight > 0){
                similarities.put(entry.getKey(), docWeight);
                similarityScoresUnique.add(docWeight);
            }
        }
        ArrayList<Double> similarityScoresSorted = new ArrayList<Double>(similarityScoresUnique);

        Collections.sort(similarityScoresSorted);
        ArrayList<Integer> finalResults = new ArrayList<Integer>();

        if(this.k == 0){
            for (int i = 0; i < similarityScoresSorted.size(); i++) {
                finalResults.addAll(LinearSearch(similarities, similarityScoresSorted.get(similarityScoresSorted.size() - i - 1)));
            }
        }
        else{
            int minSize;
            if(this.k < similarityScoresSorted.size()) {
                minSize = k;
            }
            else {
                minSize = similarityScoresSorted.size();
            }
            
            for (int i = 0; i < minSize; i++) {
                finalResults.addAll(LinearSearch(similarities, similarityScoresSorted.get(similarityScoresSorted.size() - i - 1)));
            }
        }
        

        return finalResults;
    }

    private ArrayList<Integer> calculationMethod2(List<String> list) { // the list contains all the terms of the query

        HashMap<Integer, Double> similarities = new HashMap<Integer, Double>();        //docNum,w

        // gia ka8e oro
        for (int i = 0; i < list.size(); i++) {
            String term = list.get(i);

            HashMap<Integer, Integer> hm = ii.searchTerm(term);
            if (hm != null) {


                if (!hm.isEmpty()) {    // if the term exists in the inderted index

                    // calculate the IDF
                    int nt = hm.size();
                    int N = ii.getNumberOfDocuments();
                    double IDF = Math.log(N / nt) / Math.log(N);    // method 2: 3rd logarithmic formation

                    // calculate the TFs

                    for (Entry<Integer, Integer> entry : hm.entrySet()) {
                        double tmp = (double) Math.log(entry.getValue()) + 1; // method 2: logarithmic formation
                        double w = tmp * IDF;
                        if (similarities.containsKey(entry.getKey())) {
                            Double freq = similarities.remove(entry.getKey());
                            freq += w;
                            similarities.put(entry.getKey(), freq);
                        } else {
                            similarities.put(entry.getKey(), w);
                        }
                    }
                }
            }
        }

        HashSet<Double> similarityScoresUnique = new HashSet<Double>();

        for (Entry<Integer, Double> entry : similarities.entrySet()) {
            double tmp2 = ii.getUniqTermsForDoc(entry.getKey());
            double docWeight = (double) entry.getValue() / tmp2; //ii.getLengthForDoc(entry.getKey());
            similarities.put(entry.getKey(), docWeight);
            similarityScoresUnique.add(docWeight);
        }
        ArrayList<Double> similarityScoresSorted = new ArrayList<Double>(similarityScoresUnique);

        Collections.sort(similarityScoresSorted);
        ArrayList<Integer> finalResults = new ArrayList<Integer>();

        for (int i = 0; i < k; i++) {
            finalResults.addAll(LinearSearch(similarities, similarityScoresSorted.get(similarityScoresSorted.size() - i - 1)));
        }

        return finalResults;

    }

    private List<Integer> LinearSearch(HashMap<Integer, Double> similarities, Double num) {
        ArrayList<Integer> results = new ArrayList<Integer>();

        for (Entry<Integer, Double> entry : similarities.entrySet()) {
            if (entry.getValue().equals(num)) {
                results.add(entry.getKey());
            }
        }
        return results;
    }

    @Override
    public void userQuery() {
        this.k = Menu.askForTopk();
        int calcMethod = Menu.askForCalculationMethod();
        List<String> terms = F2S.tokenizer(Menu.askForUserQuery());
        List<Integer> results = null;

        long startTime = System.currentTimeMillis();
        switch (calcMethod) {
            case 1:
                results = calculationMethod1(terms);
                break;
            case 2:
                results = calculationMethod2(terms);
                break;
        }
        
        long stopTime = System.currentTimeMillis();
        long runTime = stopTime - startTime;
        System.out.println("Run time: " + runTime);
        System.out.println(results);
    }

    @Override
    public void fileQuery() {
        
        this.k = Menu.askForTopk();
        int calcMethod = Menu.askForCalculationMethod();
        String path = Menu.askForFileQuery();
        BufferedReader br;
        String strLine;
        int statisticsChoise = Menu.askForStatistics();
        switch (statisticsChoise) {
            case 1:
                statistics = new Statistics("MEDRelevent.txt");
                break;
            case 2:
                statistics = new Statistics("CRANRelevent.txt");
                break;
            case 0:
                break;
        }
        
        try {
            FileInputStream fstream = new FileInputStream(path);
            DataInputStream in = new DataInputStream(fstream);
            br = new BufferedReader(new InputStreamReader(in));
            int i = 1;
            float sumPrecision = 0;
            float sumRecall = 0;
            
            switch (calcMethod) {
                case 1:
                    while ((strLine = br.readLine()) != null) {
                        
                        long startTime = System.currentTimeMillis();
                        
                        List<String> terms = F2S.tokenizer(strLine);
                        List<Integer> results = calculationMethod1(terms);
                        System.out.println("line " + i + " -> " + results);
                        
                        long stopTime = System.currentTimeMillis();
                        long runTime = stopTime - startTime;
                        System.out.println("Run time: " + runTime);

                        
                        if(statisticsChoise != 0){
                            System.out.println("precision: "+statistics.getPrecision(i, results)+", recall: "+statistics.getRecall(i, results));
                            sumPrecision += statistics.getPrecision(i, results);
                            sumRecall += statistics.getRecall(i, results);
                        }
                        i++;
                    }
                    break;
                case 2:
                    while ((strLine = br.readLine()) != null) {

                        long startTime = System.currentTimeMillis();
                        
                        List<String> terms = F2S.tokenizer(strLine);
                        List<Integer> results = calculationMethod2(terms);
                        System.out.println("line " + i + " -> " + results);
                        
                        long stopTime = System.currentTimeMillis();
                        long runTime = stopTime - startTime;
                        System.out.println("Run time: " + runTime);

                        
                        System.out.println("precision: "+statistics.getPrecision(i, results)+", recall: "+statistics.getRecall(i, results));
                        sumPrecision += statistics.getPrecision(i, results);
                        sumRecall += statistics.getRecall(i, results);
                        i++;
                    }
                    break;
            }
            
            System.out.println("\nAverage Precision:\t" + sumPrecision/i);
            System.out.println("Average Recall:\t\t" + sumRecall/i);

            in.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
        
        
        
    }
}
