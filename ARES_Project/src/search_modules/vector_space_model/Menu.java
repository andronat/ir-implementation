/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package search_modules.vector_space_model;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 *
 * @author ilias
 */
class Menu {
    
    public static int ChooseInput(){
        
        int selection;
        System.out.println("1:\tfrom a file");
        System.out.println("2:\tfrom an input");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Choose: ");
        selection = scanner.nextInt();

        return selection;
    }
    
    static int askForCalculationMethod() {
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        int selection = 0;

        System.out.println("\nChoose calculation method:\n");
        System.out.println("1. method 1");
        System.out.println("2. method 2");

        while (flag) {
            try {
                System.out.print("Insert number of choise: ");
                selection = scanner.nextInt();
                flag = false;
            } catch (InputMismatchException e) {
                System.out.println("Wrong input.\n");
            }
        }

        return selection;
    }
    
    static String askForUserQuery() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nGive query:\n");
        return scanner.nextLine();
    }

    static String askForFileQuery() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nGive the path of the input file:\n");
        return scanner.nextLine();
    }

    static int askForTopk() {
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        int selection = 0;

        System.out.println("Do you want to set a topk limit? (yes/no)");
        boolean answer = (scanner.next(Pattern.compile("yes|no")).equals("yes")) ? true : false;
        
        if(answer){
            System.out.println("\nHow many results do you want to display (topk)?\n");

            while (flag) {
                try {
                    System.out.print("Give amount: ");
                    selection = scanner.nextInt();
                    flag = false;
                } catch (InputMismatchException e) {
                    System.out.println("Wrong input.\n");
                }
            }

            return selection;
        }
        else{
            return 0;
        }
    }
    
    static int askForStatistics() {
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        int selection = 0;

        System.out.println("Do you want to display statistics? (yes/no)");
        boolean answer = (scanner.next(Pattern.compile("yes|no")).equals("yes")) ? true : false;

        if (answer) {
            System.out.println("\nChoose a library\n");
            System.out.println("1. MED");
            System.out.println("2. CRAN");
            while (flag) {
                try {
                    System.out.print("Insert number of choise: ");
                    selection = scanner.nextInt();
                    flag = false;
                } catch (InputMismatchException e) {
                    System.out.println("Wrong input.\n");
                }
            }

            return selection;
        } else {
            return 0;
        }
    }
}
