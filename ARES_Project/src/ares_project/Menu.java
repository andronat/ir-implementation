/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ares_project;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 *
 * @author Anastasis Andronidis <anastasis90@yahoo.gr>
 */
class Menu {
    static boolean askToOpenNewCreatedCollection() {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Do you want to open this collection? (yes/no)");
        return (scanner.next(Pattern.compile("yes|no")).equals("yes")) ? true : false;
    }
    
    static int printSecondaryMenu() {
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        int selection = -1;
        
        System.out.println("\n\nYou have an active collection." + "\nChoose:");
        System.out.println("\n1.Add a file to collection.");
        System.out.println("2.Add files to collection. (give a directory path)");
        System.out.println("3.Run a query.");
        System.out.println("4.Run query(s) from file.");
        System.out.println("5.Close collection.");
        
        while (flag) {
            try {
                System.out.print("Insert number of choise: ");
                selection = scanner.nextInt();
                flag = false;
            } catch (InputMismatchException e) {
                System.out.println("Wrong input.\n");
            }

        }
        
        return selection;
    }
    
    static int printMainMenu() {
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        int selection = -1;

        System.out.println("\n\nWelcome to IR project." + "\nChoose:");
        System.out.println("\n1.Open collection");
        System.out.println("2.Create collection.");
        System.out.println("3.Delete collection.");
        System.out.println("99.EXIT");

        while(flag) {
            try{
                System.out.print("Insert number of choise: ");
                selection = scanner.nextInt();
                flag = false;
            } catch (InputMismatchException e) {
                System.out.println("Wrong input.\n");
            }
   
        }
        
        return selection;
    }

    static String askCollectionName() {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("\nGive collection name:\n");
        return scanner.nextLine();
    }

    static int askForSearchModel() {
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        int selection = 0;

        System.out.println("\nChoose search model to apply to index:\n");
        System.out.println("1. Vector Space Model.");
        System.out.println("2. Boolean Model.");
        
        while (flag) {
            try {
                System.out.print("Insert number of choise: ");
                selection = scanner.nextInt();
                flag = false;
            } catch (InputMismatchException e) {
                System.out.println("Wrong input.\n");
            }
        }
        
        return selection;
    }
}
