/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ares_project;

import ares_project.utilities.F2S;
import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Anastasis Andronidis <anastasis90@yahoo.gr>
 */
public class InvertedIndex implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private HashMap<String,HashMap<Integer,Integer>> invertedIndexMap = new HashMap<String,HashMap<Integer,Integer>>();
    
    private HashMap<Integer, Integer> docsLengthMap = new HashMap<Integer, Integer>();
    private HashMap<Integer, Integer> docsUniqTermMap = new HashMap<Integer, Integer>();
    
    public InvertedIndex() {   
    }
    
    private boolean addTerm(String word, Integer docNum) {

        try {
            if (this.invertedIndexMap.containsKey(word)) {
                HashMap<Integer, Integer> docList = this.invertedIndexMap.get(word);

                if (docList.containsKey(docNum)) {
                    Integer wordFrq = docList.get(docNum);
                    docList.put(docNum, ++wordFrq);
                } else {
                    docList.put(docNum, 1);
                    
                    // Uniq terms for each doc.
                    if(this.docsUniqTermMap.containsKey(docNum)){
                        Integer numberOfUniqTerms = docsUniqTermMap.get(docNum);
                        numberOfUniqTerms++;
                        this.docsUniqTermMap.put(docNum, numberOfUniqTerms);
                    } else {
                        this.docsUniqTermMap.put(docNum, 1);
                    }
                }

            } else {
                HashMap<Integer, Integer> tmp = new HashMap<Integer, Integer>();
                tmp.put(docNum, 1);
                this.invertedIndexMap.put(word, tmp);
                
                
                // Uniq terms for each doc.
                if (this.docsUniqTermMap.containsKey(docNum)) {
                    Integer numberOfUniqTerms = this.docsUniqTermMap.get(docNum);
                    numberOfUniqTerms++;
                    this.docsUniqTermMap.put(docNum, numberOfUniqTerms);
                } else {
                    this.docsUniqTermMap.put(docNum, 1);
                }
            }

            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }       

    private boolean addTerm(String word, String docName) {
        Integer docNum;
        try {
            docNum = Integer.parseInt((docName.split(".txt")[0].trim()));
        } catch (Exception ex) {
            Logger.getLogger(InvertedIndex.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

        if (addTerm(word, docNum)) {
            return true;
        } else {
            return false;
        }
    }
    
    public int getNumberOfDocuments(){
        return this.docsLengthMap.size();
    }
    
    public HashMap<Integer,Integer> searchTerm(String term){
        
        HashMap<Integer,Integer> listOfdocs = null;
                
        if(invertedIndexMap.containsKey(term)) {
            listOfdocs = invertedIndexMap.get(term);
        }
        
        return listOfdocs;
    }

    boolean addDocument(File newDoc) {
        String docName = newDoc.getName();
        Integer docNum;
        
        try {
            docNum = Integer.parseInt((docName.split(".txt")[0].trim()));
        } catch (Exception ex) {
            Logger.getLogger(InvertedIndex.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
        String fileString = F2S.fileToString(newDoc);
        List<String> tokens = F2S.tokenizer(fileString);
        
        
        docsLengthMap.put(docNum, tokens.size());
        
        for (String token : tokens) {
            addTerm(token, docNum);
        }
        
        return true;
    }

    boolean addDocuments(List<File> newDocs) {
        for (File d : newDocs){
            addDocument(d);
        }
        return true;
    }   
    
    public int getLengthForDoc(int docNum) {
        return docsLengthMap.get(docNum);
    }
    
    public Integer getUniqTermsForDoc(Integer docNum) {
        return docsUniqTermMap.get(docNum);
    }
    
    public Set<Integer> getAllDocs() {
        return docsLengthMap.keySet();
    }
}