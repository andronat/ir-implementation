/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ares_project;

import ares_project.utilities.CollectionFileManager;
import java.io.File;
import java.io.IOException;
import java.util.List;
import search_modules.SearchModel;
import search_modules.boolean_model.BooleanModel;
import search_modules.vector_space_model.VectorSpaceModel;

/**
 *
 * @author Anastasis Andronidis <anastasis90@yahoo.gr>
 */
class IndexManager {
    
    private InvertedIndex ii = null;
    private SearchModel sm = null;
    private String collectionName = null;
    
    private void createCollection() {
        this.collectionName = CollectionFileManager.createCollectionDirectory();
        
        if (Menu.askToOpenNewCreatedCollection()) {
            openCollection(this.collectionName);
        }
    }
    
    private void deleteCollection() {
        this.collectionName = Menu.askCollectionName();
        
        CollectionFileManager.deleteCollectionFiles(this.collectionName);
    }

    private void openCollection() {
        this.collectionName = Menu.askCollectionName();
        
        this.ii = CollectionFileManager.loadInvertedIndexFromFile(this.collectionName);
    }
    
    private void openCollection(String collectionName) {
        this.ii = CollectionFileManager.loadInvertedIndexFromFile(collectionName);
    }
    
    private void closeCollection() {
        CollectionFileManager.writeInvertedIndexToFile(this.ii, this.collectionName);
        this.ii = null;
        this.sm = null;
    }
    
    private void loadSearchModel() {
        int selection = Menu.askForSearchModel();
        
        switch (selection) {
            case 1:
                this.sm = new VectorSpaceModel(this.ii);
                break;
            case 2:
                this.sm = new BooleanModel(this.ii);
                break;
        }
    }
    
    void userMenu() throws IOException {
        int selection;
        
        while(true){
            // Main menu. Asking for user to open, to create or delete a collection
            if (this.ii == null) {
                selection = Menu.printMainMenu();

                switch (selection) {
                    case 1:
                        // Open a collection
                        openCollection();
                        break;
                    case 2:
                        // Create a collection
                        createCollection();
                        break;
                    case 3:
                        // Create a collection
                        deleteCollection();
                        break;
                    case 99:
                        // Exit the program
                        return;
                }
            } else if (this.sm == null) {
                // Asks the user to choose a search model to use on the inverted index.
                loadSearchModel();
            } else {
                // Asks the user to choose one of the main opperations on inverted index.
                selection = Menu.printSecondaryMenu();
                
                switch (selection) {
                    case 1:
                        // Add a file to the collection and then in the inverted index. 
                        // (Parse the file and adds all the words).
                        File newFile = CollectionFileManager.addFileToCollectionFolder(this.collectionName);
                        this.ii.addDocument(newFile);
                        break;
                    case 2:
                        // Add multiple files to the collection and then in the inverted index. 
                        // (Parse the files and adds all the words).
                        List<File> newFiles = CollectionFileManager.addFilesToCollectionFolder(this.collectionName);
                        this.ii.addDocuments(newFiles);
                        break;
                    case 3:
                        // Run queries based an the search model the user added before.
                        this.sm.userQuery();
                        break;
                    case 4:
                        this.sm.fileQuery();
                        // Run queries from a files.
                        break;
                    case 5:
                        // Close the collection.
                        closeCollection();
                }
            }
        }
    }
}
