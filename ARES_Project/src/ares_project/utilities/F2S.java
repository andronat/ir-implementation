/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ares_project.utilities;

/**
 *
 * @author Anastasis Andronidis <anastasis90@yahoo.gr>
 */
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class F2S {
    
    public static String fileToString(File file) {
        String result = null;
        byte[] buffer = new byte[(int) file.length()];

        try {
            DataInputStream in = new DataInputStream(new FileInputStream(file));
            in.readFully(buffer);
            result = new String(buffer);

        } catch (IOException ex) {
            Logger.getLogger(F2S.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }
    
    public static List<String> tokenizer(String docString) {
        List<String> tokens = new ArrayList<String>();
        
        Pattern p = Pattern.compile("\\w+");
        Matcher m = p.matcher(docString);

        String word;
        while (m.find()) {
            word = m.group().toLowerCase();

            tokens.add(word);
        }
        
        return tokens;
    }
    
}
