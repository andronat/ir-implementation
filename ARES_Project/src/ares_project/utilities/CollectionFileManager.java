/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ares_project.utilities;

import ares_project.InvertedIndex;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Anastasis Andronidis <anastasis90@yahoo.gr>
 */
public class CollectionFileManager {
    
    private static void copyFile(File source, File dest, List<File> newFiles) throws IOException {
        if (source.isDirectory()) {

            String[] children = source.list();
            for (int i = 0; i < children.length; i++) {
                copyFile(new File(source, children[i]), new File(dest, children[i]), newFiles);
            }
        } else {
            source.renameTo(dest);
            newFiles.add(dest.getAbsoluteFile());
        }
    }
    
    private static void deleteRecursivelyFiles(File f) {
        if (f.isDirectory()) {
            for (File c : f.listFiles()) {
                deleteRecursivelyFiles(c);
            }
        }
        f.delete();
    }
    
    //DIMIOURGIA SULLOGHS
    public static String createCollectionDirectory() {

        Scanner scanner = new Scanner(System.in);
        String collectionName;

        //ELEGXOS AN DEN UPARXEI O UPOFAKELOS ME OLES TIS SULLOGES.
        //AN DEN UPARXEI TON KANEI. AN UPARXEI EPISTROFI MINIMATOS YPARKSIS.

        if (new File("MyCollections").exists()) {
            System.out.println("Folder MyCollections already exists.");
        } else {
            if ((new File("MyCollections")).mkdir()) {
                System.out.println("Folder created.");
            }
        }

        //DINEI ONOMA SULLOGHS
        System.out.println("Give collection name: ");
        collectionName = scanner.next();

        //AN UPARXEI H SULLOGH LOOP EDW GIA ALLO ONOMA
        while (new File("MyCollections/" + collectionName).exists()) {
            System.out.print("Name taken. Give a different name: ");
            collectionName = scanner.next();
        }

        //AN DEN UPARXEI H SULLOGH TH DIMIOURGOUME
        try {
            // Create one directory
            if ((new File("MyCollections", collectionName)).mkdir()) {
                System.out.println("Directory: "
                        + collectionName + " created");
                
                //KAI MESA STO SUBCATEGORY DIMIOURGIA ENOS ARXEIO .II
                InvertedIndex ii = new InvertedIndex();
                ObjectOutputStream iiFile = new ObjectOutputStream(new FileOutputStream("MyCollections/" + collectionName + "/" + collectionName + ".ii"));
                iiFile.writeObject(ii);
                iiFile.close();
            }

        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
        
        return collectionName;
    }
    
    //ANTIGRAFH ENOS ARXEIOU
    public static File addFileToCollectionFolder(String collectionName) throws IOException {
        
        Scanner scanner = new Scanner(System.in);
        
        File sourceFile, targetFile = null;
        String targetDirectory = "MyCollections/" + collectionName + "/";
        
        System.out.print("Give the path of the file of type cancel: ");
        sourceFile = new File(scanner.next());
        
        while (!sourceFile.exists()) {
            System.out.println("File not found.");
            System.out.print("Give the path of the file of type cancel: ");
            sourceFile = new File(scanner.next());
        }
        
        try {
            targetFile = new File(targetDirectory, sourceFile.getName());
            if (sourceFile.renameTo(targetFile)) {
                System.out.println("File is moved successful!");
            } else {
                System.out.println("File is failed to move!");
            }

        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
        
        return targetFile;
    }
    
    //EISAGWGH POLLWN ARXEIWN
    public static List<File> addFilesToCollectionFolder(String collectionName) throws IOException {
        Scanner scanner = new Scanner(System.in);
        List<File> newFiles = new ArrayList<File>();
        
        File sourceDirectory, 
                targetDirectory = new File("MyCollections/" + collectionName + "/");

        System.out.print("Give the path of the directory: ");
        sourceDirectory = new File(scanner.next());
        while (!sourceDirectory.exists()) {
            System.out.println("Directory not found.");
            System.out.print("Give the path of the directory: ");
            sourceDirectory = new File(scanner.next());
        }
        
        copyFile(sourceDirectory, targetDirectory, newFiles);
        
        return newFiles;
    }
  
    public static InvertedIndex loadInvertedIndexFromFile(String collectionName) {
        InvertedIndex ii = null;
        try {
            FileInputStream fileIn = new FileInputStream("MyCollections/" + collectionName + "/" + collectionName + ".ii");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            ii = (InvertedIndex) in.readObject();
            in.close();
        } catch (IOException ex) {
        } catch (ClassNotFoundException ex) {
        }
        
        return ii;
    }
    
    public static void writeInvertedIndexToFile(InvertedIndex ii, String collectionName) {
        try {
            FileOutputStream fileOut = new FileOutputStream("MyCollections/" + collectionName + "/" + collectionName + ".ii");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(ii);
            out.close();
            fileOut.close();
        } catch (IOException i) {
        }
    }

    public static void deleteCollectionFiles(String collectionName) {
        File collectionDirectory = new File("MyCollections/" + collectionName + "/");
        
        deleteRecursivelyFiles(collectionDirectory);
    }    
}
